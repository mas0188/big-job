#!/usr/bin/env bash
# NAME
#	Testing script
#
# DESCRIPTION
#	Script for testing
#
# AUTHOR
#	Dancan
#
VERSION=0
RELEASE=1
#
# CHANGELOG
# 2021-11-19 8:30 - Initial release

###############################################

echo "Hello, this is testing script, Version: $VERSION, Release: $RELEASE"



exit 0
